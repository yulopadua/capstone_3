import React, { useState, useEffect } from 'react';
import { Container, Modal, Form, Button, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';

import PropTypes from 'prop-types';

export default function Product({product, productId}) {

	const { name, description, price } = product;

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [quantity, setQuantity] = useState('');
	const [buyProductButton, setBuyProductButton] = useState(false)

	useEffect(()=>{
		if(quantity !== ''){
			setBuyProductButton(true)
		}else{
			setBuyProductButton(false)
		}
	}, [quantity])

	function viewProduct(){
		fetch(`http://localhost:4000/products/${productId}`, {
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	}

	function buyProduct(){
		/*setCount(count + 1)
		setSeats(seats - 1)*/
		fetch('https://frozen-shelf-44115.herokuapp.com/users/checkout', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					icon:'success',
					title: 'Your purchase was successful!'
				})
			}else{
				Swal.fire({
					icon:'err',
					title: 'Please login to continue.'
				})
			}
		})
	}


	return(
	<section className="products">
		<Container>
			<Card className="cardProduct">
				<Card.Body>
					<Card.Title>
						<h5>{name}</h5>
					</Card.Title>
					<Card.Text>

						<h6>Price:</h6>
						<p>₱ {price}</p>

						<Button variant="primary" onClick={()=>{handleShow(); viewProduct()}}>View</Button>

				<>

					<Modal show={show} onHide={handleClose}>
				        <Modal.Header closeButton>
				          <Modal.Title>Checkout</Modal.Title>
				        </Modal.Header>

				        <Modal.Body>
				        	<h5>{name}</h5>
				        	<h6>Description:</h6>
							<p>{description}</p>
							<h6>Price:</h6>
							<p>₱ {price}</p>
				        	<h6>Quantity:</h6>
							<input 
								min="1"
								type="number" 
								value={quantity}
								onChange={e => setQuantity(e.target.value)}
								required 
							/>
				        </Modal.Body>

				        <Modal.Footer>
				          <Button variant="secondary" onClick={handleClose}>
				            Close
				          </Button>

					        {buyProductButton ? <Button variant="primary" type="submit" onClick={buyProduct}>Buy</Button>
							:
							<Button variant="primary" type="submit" disabled>Buy</Button>
							}

				        </Modal.Footer>
			      	</Modal>
		      	</>


					</Card.Text>
				</Card.Body>
			</Card>
		</Container>
	</section>
		)

}

Product.propTypes = {
	laman: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}