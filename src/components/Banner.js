import React from 'react';
import { Container, Jumbotron } from 'react-bootstrap';

export default function Banner() {
	return(
		<Container>
			<Jumbotron>
				<h1 class="display-4 fw-bold">Welcome to Plantr</h1>
				<h3>Create your own indoor oasis</h3>
			</Jumbotron>
		</Container>
		)
}