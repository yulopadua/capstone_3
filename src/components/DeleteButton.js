import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function DeleteButton({productId}) {

	function deleteProduct(e){
		e.preventDefault();

		fetch(`https://frozen-shelf-44115.herokuapp.com/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					icon: 'success',
					title: 'Deleted!'
				})

				.then(()=>window.location.reload())
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Failed to delete'
				})
			}
		})
	}


	return(
		<Button variant="danger" onClick={deleteProduct}>Delete</Button>
		)
}