import React, { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';

export default function Register(){

	const history = useHistory();

	const { user } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');

	const [registerButton, setRegisterButton] = useState(false);

	useEffect(()=>{
		if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setRegisterButton(true)
		}else{
			setRegisterButton(false)
		}
	}, [email, password, verifyPassword])

	function registerUser(e){
		e.preventDefault();

		fetch('https://frozen-shelf-44115.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
						icon: "success",
						title: "You have successfully registered!"
				})
			}else{
				Swal.fire({
						icon: "error",
						title: "Email is already registered",
				})
			}

		history.push('/login')
		})

		setEmail('');
		setPassword('');
		setVerifyPassword('');
	}

	if(user.accessToken !== null) {
		return <Redirect to ="/" />
	}


	return(
	<section className="reg">
		<Container>
			<Form onSubmit={(e)=> registerUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control
						type="password"
						value={verifyPassword}
						onChange={e => setVerifyPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{registerButton ? <Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" type="submit" disabled>Submit</Button>
				}
				
			</Form>
		</Container>
	</section>

		)
}