import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

export default function NotFound() {
	return(
		<Container>
			<h3> Page Not Found </h3>
			<h5>Error 404</h5>
			<p>Go back to the <Link to="/">homepage</Link>.</p>
		</Container>
		)
}
