import React, { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AddProduct(){
	
	const history = useHistory();

	const { user } = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [addProductButton, setAddProductButton] = useState(true)

	useEffect(()=>{
		if(name !== '' && description !== '' && price !== 0){
			setAddProductButton(true)
		}else{
			setAddProductButton(false)
		}
	}, [name, description, price])


	function addProduct(e){
		e.preventDefault();
		
		fetch('https://frozen-shelf-44115.herokuapp.com/products/create', {
			method: 'POST',
			headers: { 
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			 },
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
						title: "Successfully Added!",
						icon: "success",
				})
				history.push('/products')

			}else{

				Swal.fire({
						title: 'Failed!',
						icon: 'error'
				})
			}

		})
	
		setName('');
		setDescription('');
		setPrice('');
}

	return(
		<section className="add">
			<Container>
				<h1>Add a Plant</h1>
				<Form onSubmit={(e)=> addProduct(e)}>
					<Form.Group controlId="productName">
						<Form.Label>Name:</Form.Label>
						<Form.Control 
							type="text" 
							placeholder="Enter Name of the Plant" 
							value={name} 
							onChange={e => setName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="productDesc">
						<Form.Label>Description:</Form.Label>
						<Form.Control 
							type="text" 
							placeholder="Enter Description" 
							value={description} 
							onChange={e => setDescription(e.target.value)} 
							required
						/>
					</Form.Group>

					<Form.Group controlId="productPrice">
						<Form.Label>Price:</Form.Label>
						<Form.Control 
							type="number"  
							value={price} 
							onChange={e => setPrice(e.target.value)}
							required/>
					</Form.Group>

					{addProductButton ? <Button variant="primary" type="submit">Submit</Button>
					:
					<Button variant="primary" type="submit" disabled>Submit</Button>
					}

				</Form>
			</Container>
		</section>
		)
}