import React, { useState, useEffect, useContext } from 'react';
import Product from '../components/Product';
import UserContext from '../UserContext';
import { Container, Table } from 'react-bootstrap';
import UpdateButton from '../components/UpdateButton';
import DeleteButton from '../components/DeleteButton';


export default function Products(){
	const { user } = useContext(UserContext);

	const [adminProducts, setAdminProducts] = useState([]);
	const [allProducts, setAllProducts] = useState([]);

	useEffect(()=>{
		if(!user.isAdmin) {
			fetch('https://frozen-shelf-44115.herokuapp.com/products/active')
			.then(res => res.json())
			.then(userProducts =>{
				console.log(userProducts)

				setAllProducts(userProducts.map(buyProducts => {
					return(
						<Product key={buyProducts._id} product={buyProducts} productId={buyProducts._id} />
						)
				}))
			})
		}else{
			fetch('https://frozen-shelf-44115.herokuapp.com/products/active')
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setAdminProducts(data.map( product => {
					return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.price}</td>
						<td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Active" : "Inactive"}</td>
						<td><UpdateButton productId={product._id} /></td>
						<td><DeleteButton productId={product._id} /></td>
					</tr>
						)
				}))
			})
		}
	}, [])

	if(!user.isAdmin){
		return(
			[allProducts]
			)
	}else{
		return(
		<section className="dashboard">
			<Container>
				<h1 className="text-center mb-5">Plants Dashboard</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Price</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						{adminProducts}
					</tbody>
				</Table>
			</Container>
		</section>
		)
	}
}