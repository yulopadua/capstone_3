import React, { useState, useEffect, useContext } from 'react';
import { Container, Table } from 'react-bootstrap';


export default function UserDetails(){

			const [userDetails, setUserDetails] = useState([]);
useEffect(()=>{
			fetch('https://frozen-shelf-44115.herokuapp.com/users/details', {
				headers: {
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
			})
			.then(res => res.json())
			.then(dataUserDetails =>{
				let email = dataUserDetails.email
				let userId = dataUserDetails._id

				
				let userDetails = (
					<tr key={userId}>
						<td>{userId}</td>
						<td>{email}</td>
					</tr>
				)
				setUserDetails(userDetails)
			})
}, [])

	
		return(
			<section className="dashboard">
		<Container>
			<h1 className="text-center mb-5">Account Details</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>User ID</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					{userDetails}
				</tbody>
			</Table>
		</Container>
		</section>
			)
	
	
}