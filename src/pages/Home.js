import React from 'react';
import Banner from '../components/Banner';
import { Container } from 'react-bootstrap';


export default function Home(){
	return(
		<section className="landing">
			<Banner />
		</section>
		)
}