import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Container, Table } from 'react-bootstrap';


export default function Orders(){
	const { user } = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);
	const [userOrders, setUserOrders] = useState([]);

	useEffect(()=>{
		if(!user.isAdmin) {
			fetch('https://frozen-shelf-44115.herokuapp.com/users/my-orders', {
				headers: {
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
			})
			.then(res => res.json())
			.then(dataUserOrders =>{
				console.log(dataUserOrders)

				setUserOrders(dataUserOrders.order.map(viewUserOrders => {
					return (
						<tr key={viewUserOrders._id}>
							<td>{viewUserOrders.productId}</td>
							<td>{viewUserOrders.purchasedOn}</td>
							<td>{viewUserOrders.totalAmount}</td>
						</tr>
					)
				}))
			})
		}else{
			fetch('https://frozen-shelf-44115.herokuapp.com/users/orders', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(dataAllOrders => {
			console.log(dataAllOrders)

			setAllOrders(dataAllOrders.map(viewAllOrders => {
					let orders = viewAllOrders.order;
					if(orders.length) {
						return orders.map(order => {
							return (
								<tr key={order._id}>
									<td>{order._id}</td>
									<td>{order.purchasedOn}</td>
									<td>{order.totalAmount}</td>
								</tr>
							)
						})
					}
			}))
		})
		}
	}, [])

	if(!user.isAdmin){
		return(
		<section className="orders">
			<Container>
				<h1 className="text-center mb-5">My Orders</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Product ID</th>
							<th>Date of Purchase</th>
							<th>Total Amount</th>
						</tr>
					</thead>
					<tbody>
						{userOrders}
					</tbody>
				</Table>
			</Container>
		</section>
			)
	}else{
		return(
		<section className="orders">
			<Container>
				<h1 className="text-center mb-5">All Orders</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Product ID</th>
							<th>Date of Purchase</th>
							<th>Total Amount</th>
						</tr>
					</thead>
					<tbody>
						{allOrders}
					</tbody>
				</Table>
			</Container>
		</section>
		)
	}
}