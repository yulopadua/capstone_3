import React, { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';

export default function Login(){

	const history = useHistory();

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [loginButton, setLoginButton] = useState(false)

	useEffect(()=>{
		if((email !== '' && password !== '')){
			setLoginButton(true)
		}else{
			setLoginButton(false)
		}
	}, [email, password])


	function loginUser(e){
		e.preventDefault();

		fetch('https://frozen-shelf-44115.herokuapp.com/users/authenticate', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data === null){
				Swal.fire({
					icon: 'error',
					title: 'Email is not registered!'
				})
			}else if(data === false){
				Swal.fire({
					icon: 'error',
					title: 'Incorrect password!'
				})
			}else{
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });

				Swal.fire({
					icon: 'success',
					title: 'You have successfully logged in!'
				})

				fetch('https://frozen-shelf-44115.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data.isAdmin === true ){
						localStorage.setItem('email', data.email);
						localStorage.setItem('isAdmin', data.isAdmin);
						localStorage.setItem('_id', data._id);

						setUser({
							email: data.email,
							isAdmin: data.isAdmin,
							_id: data._id
						})

						history.push('/products')

					}else{
						history.push('/')
					}
			
			setEmail('');
			setPassword('');
		})
	}
})

}


	if(user.accessToken !== null) {
		return <Redirect to ="/" />
	}

	return(
	<section className="login">
		<Container>
			<Form onSubmit={(e)=> loginUser(e)}>
				<Form.Group controlId="loginEmail">
					<Form.Label>Email Address:</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter email" 
						value={email} 
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="loginPassword">
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Enter password" 
						value={password} 
						onChange={e => setPassword(e.target.value)} 
						required
					/>
				</Form.Group>

				{loginButton ? <Button variant="primary" type="submit">Login</Button>
				:
				<Button variant="primary" type="submit" disabled>Login</Button>
				}
				
			</Form>
		</Container>
	</section>

		)
}