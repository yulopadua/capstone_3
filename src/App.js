import React, { useState } from 'react';

import './App.css';
import NavBar from './components/NavBar';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import AddProduct from './pages/AddProduct';
import Orders from './pages/Orders';
import UserDetails from './pages/UserDetails';
import NotFound from './pages/NotFound';

import UserContext from './UserContext';


function App() {

  const [user, setUser] = useState({ 
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({ 
      accessToken: null,
      isAdmin: null
    })
  }

  return (
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <Router>
        <NavBar user={user} />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/addProduct" component={AddProduct} />
            <Route exact path="/details" component={UserDetails} />
            <Route exact path="/orders" component={Orders} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route component={NotFound} />
          </Switch>
      </Router>
    </UserContext.Provider>
  );
}

export default App;